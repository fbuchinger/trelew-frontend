import os
import sys
from subprocess import Popen, PIPE, STDOUT

ALLOWED_EXTENSIONS = set(['cr2', 'CR2', 'nef', 'rw2', 'dng', 'orf'])
UPLOAD_FOLDER = os.path.join(os.path.dirname(__file__) ,'sample-images')
PROCESSED_FOLDER = os.path.join(os.path.dirname(__file__) ,'hosted-images')
VIPS = r"D:\dev\vips-dev-7.32.0\bin\vips.exe"

def is_raw_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
           
def tiffpath(raw_filename):
    return os.path.join(UPLOAD_FOLDER, os.path.splitext(raw_filename)[0] + '.tiff')
    
def render_raw (raw_filename):
    if (os.path.exists(raw_filename)):
        created_tiff = os.path.splitext(raw_filename)[0] + '.tiff'
        if (os.path.exists(created_tiff)):
            return created_tiff
        else:
            dcraw_output = Popen(["dcraw.exe", '-T','-q', '0', raw_filename], stdout=PIPE, stderr=STDOUT).communicate()[0]
            return created_tiff
    else:
        return None
      
def render_zoomify (tiff_filename):
    output_dir =  os.path.join(PROCESSED_FOLDER, os.path.split(tiff_filename)[1])
    output = Popen([VIPS, 'dzsave', tiff_filename, output_dir ,'--layout', 'zoomify',  '--background', '0', '--centre'], stdout=PIPE, stderr=STDOUT).communicate()[0]
    if(os.path.exists(output_dir)):
        return output_dir
        
def render_resolutions (tiff_filename):
    # TODO: render the different resolutions for the image (e.g. 640x480,...) with PIL
    pass

def process_file(filename):
    input_file = os.path.join(UPLOAD_FOLDER, filename)
    if os.path.splitext(filename)[1] != '.tiff':
        vips_input = render_raw(input_file)
    else:
        vips_input = input_file
    output_dir = render_zoomify(vips_input)
    
if __name__ == "__main__":
    rawfiles = [ f for f in os.listdir(UPLOAD_FOLDER) if os.path.isfile(os.path.join(UPLOAD_FOLDER,f)) and is_raw_file(f) ]
    for rawfile in rawfiles:
        process_file(rawfile)