import os
import sys
import json
import datetime
from subprocess import Popen, PIPE, STDOUT

from PIL import ImageEnhance
import Image

VIPS = r"D:\dev\vips-dev-7.32.0\bin\vips.exe"

class TrelewJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj,datetime.datetime):
            return obj.isoformat()
            # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)
           
def is_jpeg_file (fname):
    return os.path.splitext(fname)[1].lower() in ['.jpg', '.jpe', '.jpeg']
      
def render_zoomify (srcfile, output_folder):
    output_dir =  os.path.join(output_folder, 'zoomify')
    output = Popen([VIPS, 'dzsave', srcfile, output_dir ,'--layout', 'zoomify',  '--background', '0', '--centre'], stdout=PIPE, stderr=STDOUT).communicate()[0]
    if(os.path.exists(output_dir)):
        return output_dir
      
def modification_date(filename):
    t = os.path.getmtime(filename)
    return datetime.datetime.fromtimestamp(t)
    
def make_fast_thumbnail (srcfile, outfile, dimensions):
    im = Image.open(srcfile)
    # resize the image to the double thumbnail size with fast nearest method
    # then use the slow ANTIALIAS method for increased performance
    if dimensions[0] < im.size[0]/2:
        double_size = tuple(i * 2 for i in dimensions)
        im.thumbnail(double_size, Image.BILINEAR)
    im.thumbnail(dimensions, Image.ANTIALIAS)
    sharpener = ImageEnhance.Sharpness(im)
    sharpened_image = sharpener.enhance(1.6)
    sharpened_image.save(outfile, "JPEG")
        
def render_resolutions (srcfile, output_folder):
    # TODO: render the different resolutions for the image (e.g. 640x480,...) with PIL
    output_dir =  os.path.join(output_folder, 'jpeg')
    os.makedirs(output_dir)
    im = Image.open(srcfile)
    output_resolutions = [(75, 75), (150, 150), (640, 640)]
    output_resolutions.extend([(im.size[0]/2, im.size[1]/2, 'half'), (im.size[0], im.size[1], 'full')])
    for res in output_resolutions:
        im = Image.open(srcfile)
        if (len(res) == 2):
            outfile = os.path.join(output_dir, ('%sx%s.jpg' % res))
        elif (len(res) == 3):
            outfile = os.path.join(output_dir, ('%s.jpg' % res[2]))
        make_fast_thumbnail (srcfile, outfile, res)
        
def get_metadata (srcfile):
    im = Image.open(srcfile)
    metadata = {}
    metadata['filename'] = os.path.split(srcfile)[1]
    metadata['creationdate'] = modification_date(srcfile)
    metadata['width'] = im.size[0]
    metadata['height'] = im.size[1]
    return metadata

def process_file(filename, output_folder):
    base_file = os.path.split(filename)[1]
    imagefolder = os.path.join(output_folder, base_file)
    os.makedirs(imagefolder)
    render_zoomify(filename, imagefolder)
    render_resolutions (filename, imagefolder)
    
if __name__ == "__main__":
    input_folder = sys.argv[1]
    output_folder = sys.argv[2]
    
    if (os.path.isdir(input_folder) and os.path.isdir(output_folder)):
        metadata = []
        for file in os.listdir(input_folder):
            filepath = os.path.join(input_folder, file)
            if os.path.isfile(filepath) and is_jpeg_file(file):
                # TODO: generate index.json
                print "Processing file: %s" % file
                process_file(filepath, output_folder)
                metadata.append(get_metadata(filepath))
        
        index_json = open(os.path.join(output_folder,'index.json'),'w')
        json.dump(metadata, index_json, cls=TrelewJsonEncoder)