app.controller('StandinCtrl', function ($scope){
    $scope.photos = [
        {
            name: 'canon_eos_7d_05.cr2',
            preview_checksums: ['676e6f35cfc173f73fea9fe27699cf8185397f0c']
        }
    ];
    $scope.image = null;
    $scope.fileName = null;
    $scope.previewRead = false;
    $scope.imageFileName = '';
    $scope.sha1sum = '123';
    $scope.previewWidth = 0;
    $scope.previewHeight = 0;
    $scope.originalFileName = 'foobar.cr2';
    $scope.resultAvailable = false;
    
    $scope.getOriginalByChecksum = function (){
        var photo =_.find($scope.photos, function(photo){
            return _.include(photo.preview_checksums, $scope.sha1sum);
        });
        if (photo){
            return photo.name;
        }
        return false;
    }
    
    $scope.openInViewer = function (){
        var original = this.getOriginalByChecksum();
        if (original){
            location.href = "viewer.html#/photos/" + original;
        }
    }
});
