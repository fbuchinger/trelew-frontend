
var tiledViewer, downloadViewer;
var photo = {
    width: 6016,
    height: 4016,
    numZoomLevels: 6, //from zoomify.numberOfTiers
    maxResolution: 32,
    fullsizeinKBytes: 5080,
    kBytesPerTile: 15
};

$(function(){
    $('.start-viewertest').one('click',function(){
        initDownloadViewer();
        initZoomifyViewer();
        $(this).attr('disabled', true);

    });
 });


 function initZoomifyViewer(){
   /* First we initialize the zoomify pyramid (to get number of tiers) */
   var initTime = (new Date).getTime();
   var zoomify_width = 6016;
   var zoomify_height = 4016;
   var zoomify_url = "../imagery/users/lucasartoni/vienna2012/bike.jpg/zoomify/";
   var zoomify = new OpenLayers.Layer.Zoomify( "Zoomify", zoomify_url,
        new OpenLayers.Size( zoomify_width, zoomify_height ) );

   /* Map with raster coordinates (pixels) from Zoomify image */
    var options = {
        maxExtent: new OpenLayers.Bounds(0, 0, zoomify_width, zoomify_height),
        maxResolution: Math.pow(2, zoomify.numberOfTiers-1 ),
        numZoomLevels: zoomify.numberOfTiers,
        units: 'pixels'
    };
    
    var isFirstLoad = true;
    zoomify.events.register("loadend", zoomify, function() {
                var loadEndTime = (new Date).getTime();
                if(isFirstLoad){
                    $('.tiled-viewer .load-time').text((loadEndTime - initTime) + 'ms');
                    //average tile size 15kb, multiplied by number of tiles
                    $('.tiled-viewer .bytes-transferred').text((photo.kBytesPerTile* zoomify.tierSizeInTiles.length));
                    $('#early-adopters-signup').show();
                    isFirstLoad = false;
                }
            });

    tiledViewer = new OpenLayers.Map($('.tiled-viewer .viewer')[0], options);
    tiledViewer.addLayer(zoomify);

    tiledViewer.setBaseLayer(zoomify);
    tiledViewer.zoomTo(2);
    //TODO: init viewer
};

function initDownloadViewer(){
     var initTime = (new Date).getTime();
        var options = {
        maxExtent: new OpenLayers.Bounds(0, 0, photo.width, photo.height),
        maxResolution: photo.maxResolution,
        numZoomLevels: photo.numZoomLevels,
        units: 'pixels'
    };
    downloadViewer = new OpenLayers.Map($('.download-viewer .viewer')[0], options);

    var image = new OpenLayers.Layer.Image(
        'Bike',
        '../imagery/users/lucasartoni/vienna2012/bike.jpg/jpeg/full.jpg?nocache=' + initTime,
        new OpenLayers.Bounds(0, 0, photo.width, photo.height),
        new OpenLayers.Size(photo.width, photo.height),
        {
            numZoomLevels: photo.numZoomLevels,
            units: 'pixels',
            maxResolution: photo.maxResolution,
            maxExtent: new OpenLayers.Bounds(0, 0, photo.width, photo.height)
        }
    );
    
    var isFirstLoad = true;
    image.events.register("loadend", image, function() {
                var loadEndTime = (new Date).getTime();
                if(isFirstLoad){
                    $('.download-viewer .load-time').text((loadEndTime - initTime) + 'ms');
                    $('.download-viewer .bytes-transferred').text(photo.fullsizeinKBytes);
                    
                    //percentage comparison of load time
                    var tileLoadTime = parseInt($('.tiled-viewer .load-time').text());
                    var downloadTime = loadEndTime - initTime;
                    if (! isNaN(tileLoadTime)){
                        var percentFaster = (100 - (tileLoadTime/(downloadTime/100))).toFixed(2);
                        $('#faster-load').parent().show();
                        $('#faster-load').text(percentFaster);
                    }

                    //comparison of data transfer
                    var tileTransfer = tiledViewer.layers[0].tierSizeInTiles.length * photo.kBytesPerTile;
                    if (! isNaN(tileTransfer)){
                        var percentLess = (100 - (tileTransfer/(photo.fullsizeinKBytes/100))).toFixed(2);
                        $('#less-data').parent().show();
                        $('#less-data').text(percentLess);

                    }
                    isFirstLoad = false;
                }
            });

    downloadViewer.addLayers([image]);
    downloadViewer.zoomTo(2);
}