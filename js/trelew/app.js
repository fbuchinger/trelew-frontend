var myApp = angular.module('trl', ['ngRoute','ui.bootstrap']);
myApp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.
      //when('/photos', {templateUrl: 'js/trelew/partials/photo-catalog.html',   controller: 'PhotoCatalogCtrl'}).
      //when('/photos/:photoName', {templateUrl: 'js/trelew/partials/photo-viewer.html', controller: 'PhotoViewerCtrl'}).
      when('/user/:userName/albums', {templateUrl: 'js/trelew/partials/user-albums.html', controller: 'UserAlbumsCtrl'}).
      when('/user/:userName/album/:albumFolder', {templateUrl: 'js/trelew/partials/photo-viewer.html', controller: 'UserAlbumCtrl'}).
      when('/user/:userName/album/:albumFolder/:photoName', {templateUrl: 'js/trelew/partials/photo-viewer.html', controller: 'UserAlbumCtrl'})

      //.otherwise({redirectTo: '/photos'});
}]);

//TODO: remove this to directives/photoviewer.js after checking module/scope troubles
myApp.directive('photoViewer', function($parse, $timeout) {
        return {
            restrict: 'EA',
			priority: -10,
			link: function (scope, elem, attrs){
                scope.$watch('photoName',function(){
                    if (scope.photoName === undefined){
                        return;
                    }
                    console.log("photoName", scope.photoName);
                    //TODO:refine this to get only called once
                    var zoomifyUrl = scope.baseUrl + scope.photoName + '/zoomify/';
                    var zoomify = new OpenLayers.Layer.Zoomify( "Zoomify", zoomifyUrl,
                        new OpenLayers.Size( scope.photoWidth, scope.photoHeight ) );

                    /* Map with raster coordinates (pixels) from Zoomify image */
                    var options = {
                        maxExtent: new OpenLayers.Bounds(0, 0, scope.photoWidth, scope.photoHeight),
                        maxResolution: Math.pow(2, zoomify.numberOfTiers-1 ),
                        numZoomLevels: zoomify.numberOfTiers,
                        tileAnimation: true,
                        units: 'pixels'
                    };


                    map = new OpenLayers.Map(elem[0], options);
                    map.addLayer(zoomify);

                    map.setBaseLayer(zoomify);
                    //map.zoomToMaxExtent();
                    map.zoomTo(4);
                    var model = $parse(attrs.photoViewer);
                    //Set scope variable for the map
                    if(model){model.assign(scope, map);}
                })

            }
        }
});