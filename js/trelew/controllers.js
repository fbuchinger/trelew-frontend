/*
* TODO:
*  - use $locationchangesuccess event (http://docs.angularjs.org/api/ng.$location#$locationchangesuccess) to react on url changes
*  - separate controllers for album and photo view
*  - AJAX request code goes to a service
*  - split out templates
* */


myApp.controller('UserAlbumCtrl', function($scope, $routeParams, $http, $rootScope){

    $scope.getPhotoByName = function (name){
        var validPhotoNameInUrl = (name && _.where($scope.photos, {filename: name}).length === 1);
        var photo = (validPhotoNameInUrl ? _.where($scope.photos, {filename: name})[0] : $scope.photos[0]);
        return photo;
    }
    if ($scope.userName !== $routeParams.userName || $scope.albumFolder !== $routeParams.albumFolder){
         if ($scope.userName !== $routeParams.userName){
            $rootScope.$broadcast('userName.changed', $routeParams.userName);
        }
        if ($scope.albumFolder !== $routeParams.albumFolder){
            $rootScope.$broadcast('album.changed', $routeParams.albumFolder);
        }
        $http.get('/dev/trelew/imagery/users/'+ $routeParams.userName +'/' + $routeParams.albumFolder + '/index.json').success(function(data) {
            var photos = data;
            $scope.photos = photos;
            var photo = $scope.getPhotoByName($routeParams.photoName);
            $scope.userName = $routeParams.userName;
            $scope.albumFolder = $routeParams.albumFolder;
            $scope.photoName = photo.filename;
            $scope.photoWidth = photo.width;
            $scope.photoHeight = photo.height;
            $scope.baseUrl = '../imagery/users/'+ $scope.userName +'/' + $scope.albumFolder + '/';
            $rootScope.$broadcast('photo.changed', photo);
            $rootScope.$broadcast('photos.changed', photos);
            $scope.getLinkFor = function (downloadVersion){
                return '../imagery/' + $scope.userName +'/'+ $scope.albumFolder +'/'+ $scope.photoName + '/' + downloadVersion;
            }
        });
    }
    else {
         $rootScope.$broadcast('photo.changed', $scope.getPhotoByName($routeParams.photoName));
    }
});

myApp.controller('UserNameCtrl', function ($scope, $rootScope){
    $scope.$on('userName.changed', function(e, userName){
        $scope.userName = userName;
    })
});

myApp.controller('UserAlbumThumbnailCtrl', function ($scope, $rootScope){
    $scope.$on('photos.changed', function(e, photos){
        $scope.photos = photos;
    })
});

/*
myApp.controller('PhotoViewerCtrl', function($scope, $routeParams, $http){
  $scope.photoName = $routeParams.photoName || 'canon_eos_7d_05.cr2';
  var photo = _.where(photos, {name: $scope.photoName})[0];
  $scope.photoWidth = photo.width;
  $scope.photoHeight = photo.height;
});
*/
function TrelewHeaderCtrl ($scope){
    $scope.version = '0.0.1';
}

myApp.controller('AlbumCtrl', function($scope){
    $scope.$on('album.changed', function(e,album){
       $scope.albumName = album;
    });
})

function UserAlbumsCtrl ($scope, $routeParams, $http){
    $scope.userName = $routeParams.userName;
    $http.get('../imagery/users/'+ $scope.userName +'/albums.json').success(function(data) {
        $scope.albums = data;
    });
};


myApp.controller('DownloadCtrl', function($scope, $routeParams) {
    //$scope.watch('$routeParams', function(){
        $scope.items = [];
        $scope.$on('photo.changed', function(e, photo){
           if ($routeParams.userName && $routeParams.albumFolder && $routeParams.photoName){
              $scope.photoName = photo.filename;
              $scope.items = [
                  {link:  $scope.getLinkFor('640x640.jpg'), text: "Download as low res JPEG (640x 640)"},
                  {link:  $scope.getLinkFor('half.jpg'), text: "Download as half res JPEG "},
                  {link:  $scope.getLinkFor('full.jpg'), text: "Download as full res JPEG"},
                  {link: '', text: "Request another resolution"}
              ];
            }
        });
        $scope.getLinkFor = function (downloadVersion){
            return '../imagery/users/' + $routeParams.userName +'/'+ $routeParams.albumFolder +'/'+ $routeParams.photoName + '/jpeg/' + downloadVersion;
        }

        $scope.color = '';
    //})
});

myApp.controller('ZoomviewCtrl', function ($scope){
    $scope.items = [
    "Fit to width",
    "Fit to height",
    "Fit to window",
    "---Named views--",
    "woman on bench",
    "man looking",
    "--- Links for Admin ---",
    "Define new view [hotkey]...",
    "View Settings ...",
  ];
});

myApp.controller('TypeaheadCtrl', function ($scope){
  $scope.selected = undefined;
  $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
});

myApp.controller('AccountCtrl', function ($scope){
   $scope.items = [
    "Profile",
    "Settings",
    "Pending Requests (9)",
    "Logout"
  ];
});